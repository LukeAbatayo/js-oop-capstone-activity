class Customer {
  constructor(email) {
    this.email = email;
    this.cart = new Cart(0);
    this.orders = [];
  }

  checkOut() {
    if (this.cart.contents.length !== 0) {
      this.orders.push({
        products: this.cart.contents,
        totalAmount: this.cart.totalAmount,
      });
    } else {
      console.log('Your cart is empty!');
    }

    return this;
  }
}

class Cart {
  constructor(totalAmount) {
    this.contents = [];
    this.totalAmount = totalAmount;
  }

  addToCart(product, quantity) {
    this.contents.push({
      product,
      quantity,
    });

    return this;
  }

  showCartContents() {
    console.log(this.contents);

    return this;
  }

  updateProductQuantity(productName, newQuantity) {
    let foundProduct = this.contents.find(content => content.product.name === productName);
    foundProduct.quantity = 2;

    return this;
  }

  clearCartContents() {
    this.contents.length = 0;

    return this;
  }

  computeTotal() {
    this.contents.forEach(content => {
      this.totalAmount += content.product.price * content.quantity;
    });

    return this;
  }
}

class Product {
  constructor(name, price) {
    this.name = name;
    this.price = price;
    this.isActive = true;
  }

  archive() {
    this.isActive = false;
    return this;
  }

  updatePrice(newPrice) {
    this.price = newPrice;
    return this;
  }
}

const apple = new Product('apple', 0.49);
const oranges = new Product('oranges', 0.79);
const banana = new Product('banana', 0.19);
const mango = new Product('mango', 0.99);

const luke = new Customer('luke@mail.com');
